---
layout: posts
title: Determining properties of hyperons [talk]
---

[Lattice 2021 bulletin](https://indico.cern.ch/event/1006302/contributions/4373305/) [[PDF of slides](https://millernb.gitlab.io/hyperon-talk/presentation.pdf)]

<!--more-->

## Title
Thank my contributors.


## Why hyperons?
First, what are hyperons and why do we care about them?
- By definition, a hyperon is a baryon containing at least one strange quark but no quarks of a heavier flavor
- Historically, hyperons were first discovered around the 1960s. Gell-man observed that these baryons could be classified into a baryon octet and decuplet if the baryons were composed of partons individually obeying an SU(3) flavor $\times$ SU(2) spin symmetry. Using this picture on the left, Gell-man was able to predict the existence of the Omega hyperon, and it was soon discovered few years later.

So why do we still need to study hyperons now?
- These days we have a more comprehensive model for understanding the particle zoo -- namely, the Standard Model.
- One prediction of the Standard Model is that the CKM matrix, which describes flavor mixing by the weak interaction, should be unitary. 
- This leads to the so-called "top-row unitarity" condition. One way to study this is through hyperons, as I will elaborate on later.
- Hyperons might be stable on the order of millions of years in neutron stars. Understanding properties of hyperons is important for modeling the equation of state of neutron stars, which dictates how soft or squishy neutron stars are.
- We know that $\chi$PT works well for mesons but not necessarily baryons. Testing the convergence of chiral expressions for the hyperon mass formulae and axial charges serves as an important for heavy baryon $\chi$PT.

Why do we need the lattice?
- Nucleon structure has been well-studied experimentally. Many body bound states of nucleons are abundant (chemistry)
- Hypernuclear structure much harder to study -- requires experiments like the LHCb
- Difficulty comes from their instability
- Can't use them for scattering

Notes: 
- The baryon decuplet/octet comes from SU(6) symmetry (a superset of SU(3) flavor $\otimes$ SU(2) spin, as can be seen from either picture). We have $6 \otimes 6 \otimes 6 = 56 + \cdots$,  with the $56$ irrep decomposing as $56 \otimes 10 \otimes 4 \oplus 8 \otimes 2$. Only a single $10$ and single $8$ survive once color symmetry is considered too.

## Experimental determination of $V_{us}$
- To check tow-row unitarity, need to calculate $V_{ud}$, $V_{us}$, $V_{ub}$
- But $V_{ub}$ is negligible, so mostly just a relationship between $V_{ud}$ and $V_{us}$
- Unlike determinations of $V_{ud}$, which can be obtained purely from experiment + theory, the best estimates of $V_{us}$ require LQCD.
- Three ways to estimate: $V_{us}$: kaon, hyperon, and tau decays
- Historically hyperons were used to determine $V_{us}$, but these days the most precise determinations come from kaons
- In fact, of these three sources, the widest error currently comes from using hyperon decays 

So why bother with hyperon decays?
- First reason: the LHCb will soon give improved estimates of hyperon decay widths, which should improve this method for estimating $V_{us}$; estimate this source can be competitive if we can determine the lattice part to ~1%
- Second: although you can estimate $V_{us}$ using either Kl2 or Kl2 decays, the two sources give different results

Notes:
- tau exclusive: tau -> {pion, kaon} + neutrino
- tau inclusive: tau -> sum of all possible hadronic states + neutrino
- tau theory problems: relies on finite energy sum rule assumptions; error is probably underestimated
- $V_{us}$ from tau can also be estimated using exclusive tau decays + $F_K/F_\pi$
- Historically $V_{us}$ was determined from hyperon decays but assuming SU(3) flavor symmetry (broken by ~15%). This SU(3) flavor assumption allowed one to relate the form factors of the electric charge and magnetic moment of the baryons.

## Tension between Kl2, Kl3
- See this discrepency in this figure from the most recent FLAG review
  
[Explain plot]
- $V_{us}$ can either be determined from $F_K/F_\pi$ (diagonal band) or the 0-momentum form factor (horizontal band)
- $V_{ud}$ is determined experimentally from superallowed beta decays
- Intersection of vertical blue band with other diagonal or horizontal band yields $V_{us}$
- The two results are clearly in disagreement 
- In fact, the problem is worse (or better, depending on your perspective) as a check of unitarity
- If you assume the experimental value of $V_{ud}$ and calculate the top-row unitarity check, the $F_K/F_\pi$ determination gives a disagreement from unitarity at the 3.2 sigma level
- Likewise, the Kl3 measurement gives a disagreement at the 5.6 sigma level
- We'd like to use hyperons decays to see which of these estimates of $V_{us}$ is more likely to be correct

Notes: 
- Quoted numbers: FLAG average for $N_f = 2 + 1 + 1$; results are similar-ish for $N_f = 2 + 1$, though less drastic (2.3- and 4.3-sigma)
- Quoted numbers assume superallowed beta decay estimate; using only the lattice gives a roughly 2-sigma deviation.
- Dashed line: correlation between $V_{us}$ and $V_{ud}$ assuming SM unitarity. 

## Project goals
- Mass spectrum -- good first check
- Next axial charges, vector charges
- Finally calculate form factors
- Explain lattice

## Previous Work
Not working in a vacuum

Mass spectrum:
- Hyperon spectrum has been analyzed numerous times
- Well-known figure from BMW
- We have a different lattice setup

Axial charges:
- Less work on hyperon form factors
- First calculation of hyperon axial charge from LQCD occured in 2007, used only a single lattice spacing & pion mass (Lin)
- Lin (2018): recent work
- Lin: first extrapolation of hyperon axial charges to continuum limit; used ratio of hyperon axial charge to nucleon axial charge
- Lin used a taylor expansion in the pion mass, lattice spacing, and volume -- we plan to perform a simultaneous chiral fit, as I'll elaborate on later
- Compared to Lin, we will benefit from having more ensembles available in our analysis, including more at the physical pion mass

Vector form factors:
- Not shown here
- Work by Sasaki (2017) and Shanahan et al (2015) on the hyperon transition vector form factors

Notes: 
- Lin used ratio instead: (1) cancellations between numerator and denominator, (2) avoid uncertainties from renomalization of the axial-current operator

## $\Xi` $Correlator fits
- We've fit most of the hyperon correlators for the ensembles we have
- Stability plot on right
- Masses of Xi vs $m_pi^2$ on various ensembles, ranging from $a=0.15$ to $a=0.06$ and 3 ensembles at the physical pion mass

## Fit strategy: mass formulae
Consider the S=2 hyperons
- Explain lambda_chi -- dimensionless LECs
- Examing the chiral mass formulae, we see that the formulae have many common LECs, namely the sigma terms and the axial charges
- This suggests when we perform this analysis, we should fit the hyperon mass formulae simultanously
- But this also suggests that our fit will benefit from simultaneously fitting the hyperon masses with the axial charges, which should improve the precision of both the mass extrapolations and the axial charge extrapolations

## Hyperon mass spectrum: $\Xi$ preliminary results
- We have calculated the mass spectrum for the hyperons; as an example, the results for the $\Xi$ are shown
- We perform 40 Bayesian fits and perform a model average over these models
- [Explain models]

[Explain plot]:
- Histogram of all models, sorted by what order chiral terms are included
- We see that the models that contribute the most to the model average are those without xpt terms included
- But we see that the models with xpt terms also agree 
- In summary, not yet sure whether we can say xpt is converging for these observables

## Summary
[See slide]

Notes:
- Use Feynman-Hellman fits 