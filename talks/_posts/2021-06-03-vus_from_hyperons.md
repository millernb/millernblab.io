---
layout: posts
title: $V_{us}$ from semileptonic hyperon decays [poster]
---

Director's review of the Nuclear Science Division [[PDF of poster](https://millernb.gitlab.io/poster-session-nsd/poster.pdf)]

<!--more-->

Soon LHCb will have millions of hyperon semileptonic decays available for analysis. We propose to calculate transition form factors which, when combined with measurements of decay widths from LHCb, will be used to determine the Cabibbo–Kobayashi–Maskawa (CKM) matrix element $V_{us}$. Along the way, we will also calculate the hyperon mass spectrum and axial charges as a test of baryon chiral perturbation theory, which will serve as the framework for the form factor calculations. 
