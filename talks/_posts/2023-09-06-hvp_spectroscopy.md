---
layout: posts
title: Reconstructing the late-time isovector correlation function through spectroscopy
---

[Sixth Plenary Workshop of the Muon g-2 Theory Initiative](https://indico.cern.ch/event/1258310/)  [[PDF of slides]](https://millernb-lqcd.gitlab.io/hvp-spectroscopy-talk/presentation.pdf)

<!--more-->

The hadronic vacuum polarisation contribution $a_\mu^{\rm HVP}$ can be calculated in lattice QCD from a convolution integral of the vector correlator with a known kernel function. Due to the exponential growth of the signal-to-noise ratio in the correlator at large times, the late-time regime is difficult to constrain with the necessary precision. In this talk we show how to reconstruct the isovector correlator at late times using the $I=1$ $\pi\pi$ scattering states as extracted from a dedicated lattice calculation. Results are presented for an ensemble at the physical pion mass.
