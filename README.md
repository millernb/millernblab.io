## Setting up the dev environment

Install docker and navigate to the folder where you've cloned this repo. Next run
```bash
docker pull jekyll/jekyll
docker run --rm -it -p 4000:4000 -v ${PWD}:/srv/jekyll/ jekyll/jekyll jekyll serve
```

The actual website will be generated in `/srv/jekyll/_site`, so make sure that `/_site` is in your `.gitignore`!

The website will now be available at [http://localhost:4000/](http://localhost:4000/).