---
layout: single
author_profile: true
classes: wide
#toc: true
#toc_icon: angle-right
---
# About

Hello! I'm Nolan, a practitioner of [lattice quantum chromodynamics](https://en.wikipedia.org/wiki/Lattice_QCD). I obtained my doctorate from the [University of North Carolina](https://physics.unc.edu/) under the tutelage of [Amy Nicholson](https://nicholson.web.unc.edu/).

A semi-complete list of my publications is available [here](/publications/), and a semi-complete list of my talks is available [here](/talks/).


## What is quantum chromodynamics?

| ![fundamental forces](/assets/images/about/xkcd_forces.png) |
| :--:|
| [xkcd](https://xkcd.com/1489/): "Of these four fundamental forces, there's one we don't understand." "Is it the weak force or strong--" "It's gravity." |

All known physics can be traced back to the four fundamental forces. Each of those fundamental forces, in turn, is thought to be described by a corresponding quantum field theory (with the possible exception of gravity). In order from weakest to strongest, the forces and their corresponding quantum field theories are:

| force | quantum field theory |
| --- | ---|
| gravity | unknown |
| the weak force | electroweak |
| the electromagnetic force | quantum electrodynamics  |
| the strong force | **quantum chromodynamics** |

While interesting things can undoubtedly be said about each of these forces, my research concerns quantum chromodynamics (QCD). QCD describes the intereaction of gluonic and quark fields whose excitations form hadrons (eg, neutrons and protons). Or to put in simply: QCD is the quantum field theory that explains how neutrons and protons (and some other particles) are held together.

In quantum field theory parlance, this theory of quarks and gluons is explained by a Lagrangian which obeys certain symmetries. The particular Lagrangian is

$$
\mathcal{L} = \sum_{f} \overline{q}_f \left( i \gamma^\mu D_\mu - m  \right) q_f - \frac14 G_{\mu\nu}^a G^{\mu\nu}_a \, .
$$

QCD imposes color SU(3) symmetry (the Roman indicies $a, b, c, \ldots$), which is unique to QCD, and Lorentz symmetry (the Greek indices $\mu, \nu, \ldots$), which is common to all quantum field theories. With this Lagrangian and these symmetry constraints, we can effectively explain the motion of the majority of visible matter in the universe (or, to put it less dramatically, free protons). 

## So why _lattice_ QCD?
Although QCD is described by the above Lagrangian, in practice it's very hard to use at low temperatures (think room temperature). To be technical, the problem is that the coupling constant of QCD varies with the energy (temperature), and at low energies, the coupling constant blows-up. 

For context, in introductory quantum field theory courses, we're taught how to use pertubative methods to solve quantum field theory problems, which is how we get Feynman diagrams: each Feynman diagram is a contribution to a process. Consider Compton scattering (the scattering of a photon off a charged particle). The simplest way to draw this process is through the exchange of a virtual lepton.

| ![electron-electron scattering](/assets/images/about/compton_scattering.png) |
| :---: |
| A photon scatters off an electron. Although we might intuitively think of the diagram in the top left as being how this process happens (two electrons exchange a virtual photon), all the other diagrams also contribute to this process. There are infinitely many such corrections. | 

But this isn't the _only_ way to explain Compton scattering. Alternatively, for example, the virtual lepton could split into a virtual photon and a second virtual lepton, which then recombine to form a single virtual lepton (as shown in the fourth diagram). Of course, since scattering is a quantum mechanical process, it would be more correct to say that scattering happens in a superposition of each of these possible processes.

Compton scattering is explained by quantum electrodynamics, where the coupling constant is $\alpha \approx 1/ 137$. As the processes become more complicated, more factors of $\alpha$ are introduced, which makes the process less likely. 

In QCD the coupling constant is $\alpha_S \gtrsim 1$ at low termperatures, which means that the more complicated processes actually contribute _more_ than the less complicated processes. 

Imagine what this means. Typically we think of a proton as being composed of three valence quarks communicating to each other through the exchange of virtual gluons. But `quarks -> virtual gluons -> quarks` is a simple process and is therefore not preferred at low energies; the more complicated processes (eg, `quarks -> virtual gluons -> virtual quarks -> ... -> quarks`) actually matter more. QCD at room temperature cannot merely be explained by three valence quarks. Understanding QCD requires understanding how those three quarks interact with the infinite quark-gluon sea. 

## So where does the lattice come in?

In the path integral formulation of quantum field theory, an actual QCD calculation looks something like this. 

$$
\langle O_2(t) O_1(0) \rangle = \frac{1}{Z_0} \int \mathcal{D}[q, \overline q] \mathcal{D}[A] \,  e^{i S_{\text{QCD}}[q, \overline q, A]} O_2[q, \overline q, A] O_1[q, \overline q, A]
$$

There's much that could be said about this path integral, but the important takeaway is that this integral is performed over all possible values of the quark and gluon fields at each spacetime location ($\int\mathcal{D}[q, \overline q] \mathcal{D}[A]$). That is, the integral is infinite-dimensional. 

In perturbative quantum field theory, we would expand this integral by way of Feynman diagrams. However, this trick only works when the coupling constant is less than 1 (ie, when the expansion converges). In the case of QCD at low-energies, the coupling constant is $O(1)$, so such an expansion is doomed to fail. Instead we use a trick: rather than let the fields occupy any spacetime location, we (1) only permit the fields to operate on certain sites (hence the lattice) and (2) limit our integration region to a finite-sized boxed. Now our infinite dimensional integral has been reduced to some more modestly sized integral (in lattice QCD calculations, $\sim 10^5$ lattice sites is typical).

Of course, unless your integrand is rather simple, analytically evaluating a $\sim 10^5$ dimensional integral is generally not possible. And certainly the integrand in QCD is not simple. Instead we estimate these integrals using hybrid Monte Carlo, a process which requires massive computational resources. State-of-the-art lattice QCD calculations therefore require state-of-the-art supercomputers.

---
# Recent Publications
<ul>
  {% for post in site.categories.publications limit:5 %}
    <li>
      <h2><a href="{{ post.url }}">{{ post.title }}</a></h2>
      {{ post.excerpt }}
    </li>
  {% endfor %}
</ul>