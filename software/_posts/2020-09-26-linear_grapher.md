---
layout: posts
title: spacetime-plots
excerpt: 
---
[![git repo](./../../assets/images/git_logo.png)](https://gitlab.com/millernb/linear-grapher/) 
[![Binder](./../../assets/images/binder_logo.png)](https://mybinder.org/v2/gl/millernb%2Flinear-grapher/master?filepath=spacetime_plotter.ipynb)

A python noteboook for plotting points and lines, expressly written for making [spacetime diagrams](https://en.wikipedia.org/wiki/Spacetime_diagram). To get started with a tutorial, [launch the binder instance of the notebook](https://mybinder.org/v2/gl/millernb%2Flinear-grapher/master?filepath=spacetime_plotter.ipynb).

<!--more-->

## Purpose

The purpose of this repo is to abstract away the details of creating a plot in `matplotlib`, a module which is notoriously tricky for first-time users (especially those with little or no programming experience). For instance, [per the documentation](https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/simple_plot.html#sphx-glr-gallery-lines-bars-and-markers-simple-plot-py), here is about the simplest plot one can make using `matplotlib`. 

```python
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()

plt.show()
```

|![image](https://gitlab.com/millernb/linear-grapher/-/raw/master/figs/matplotlib_simple.png)|
| :-:|
|A simple matplotlib plot|

 This "simple example", however, is already too complicated for our purposes -- we're only interested in plotting lines and points. To that end, let's plot a couple line. Recall that a line can be specified by either (1) a pair of points or (2) a point and a line. Suppose we're interested in plotting two lines with slope $-0.5$, one  passing through  $A = (1, 2)$ and the other passing through $B = (1, -1)$. Using `matplotlib`, we cannot directly plot this line -- instead, we must convert this description of a line into a collection of points, which can then be plotted via `matplotlib.pyplt.plot`. 
 
 Using either the code in this repo or `matplotlib`, we should get the following plot.
 
| ![image](https://gitlab.com/millernb/linear-grapher/-/raw/master/figs/two_lines.png) |
| :-:|
|Two lines, a grid, and a legend|
 
Here is the matplotlib implementation.

 ```python
import matplotlib 
import matplotlib.pyplot as plt
import numpy as np

# Determine range
xlim = (-5, 5)

# Line a
m_a = -0.5
A = (1, 2)
label_a = 'a'

# Line b
m_b = -0.5
B = (1, -1)
label_b = 'b'

# Convert description to arrays
for label, m, P in zip([label_a, label_b], [m_a, m_b], [A, B]):
    b = P[1] - m *P[0]
    x = np.linspace(xlim[0], xlim[1])
    y = m *x + b

    # Actually plot the line
    plt.plot(x, y, label=label, lw=2)

# Format plot to look nice
plt.xlim(xlim)
plt.ylim(xlim)
plt.gca().set_aspect('equal')
plt.minorticks_on()
plt.tick_params(direction='in', which='both')
plt.grid(which='major', alpha=0.7)
plt.grid(which='minor', alpha=0.2)

plt.legend(bbox_to_anchor=(1, 1), loc='upper left', prop={'size': 16})

plt.axhline(0, ls='--')
plt.axvline(0, ls='--')

plt.grid()
plt.show()
 ```

Compare with the much simpler implementation in this repo.
```python
from plotter import plot_diagram

# Change these
xlim = (-5, 5)
lines = [
    ('a', (1,  2), -0.5), # line a
    ('b', (1, -1), -0.5), # line b
]

# Make plot
plot_diagram(lines, xlim)
```

Of course, there are additional wrinkles to a `matplotlib` approach that must be accounted for (plotting vertical lines, specifying a line by two points instead, plotting a single point). The `matplotlib` wrapper `plot_diagram` takes care of those issues for us.
