---
layout: posts
title: lsqfit-gui
---
[![git repo](./../../assets/images/git_logo.png)](https://github.com/ckoerber/lsqfit-gui)
[![docs](./../../assets/images/docs_logo.png)](https://lsqfitgui.readthedocs.io/en/latest/)

Graphical user interface for `lsqfit` using `dash`.

<!--more-->

See [here](https://lsqfitgui.readthedocs.io/en/latest/) for the documentation.

## Install

Run
```
pip install [-e] .
```

## Usage

Either directly use a fit object to spawn a server
```python
# some_script.py
from lsqfit import nonlinear_fit
from lsqfitgui import run_server
...
fit = nonlinear_fit(data, fcn=fcn, prior=prior)
run_server(fit)
```
or use the console script entry point pointing to a `gvar` pickeled fit (and a fit function which is not stored in the pickled file)
```python
#other_script.py
import gvar as gv
from lsqfit import nonlinear_fit

def fcn(x, p):
    y = ...
    return y

...

fit = nonlinear_fit(data, fcn=fcn, prior=prior)
gv.dump(fit, "fit.p")
```
and run
```bash
lsqfitgui [--function other_script.py:fcn] fit.p
```

Both commands will spawn a local server hosting the lsqfit interface.

## Advanced configuration

It is possible to also set up fit meta information, i.e., allowing different fit models. See also the `example` directory for more details.

The following script spawns the server which generated the above image
```bash
python example/entrypoint.py
```

