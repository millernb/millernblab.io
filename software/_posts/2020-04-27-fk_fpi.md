---
layout: posts
title: $F_K/F_\pi$ from MDWF on HISQ
---
[![git repo](./../../assets/images/git_logo.png)](https://github.com/callat-qcd/project_fkfpi/tree/nolan)

Python code for our $F_K/F_\pi$ analysis.

<!--more-->