---
layout: abstract
title: The hyperon spectrum from lattice QCD
---

[PoS(Lattice2021) Volume 396 (2022)](https://doi.org/10.22323/1.396.0448){:target="_blank"}
[[arXiv:2201.01343](https://arxiv.org/abs/2201.01343){:target="_blank"}]

<!--more-->

Hyperon decays present a promising alternative for extracting $\vert V_{us} \vert$ from  lattice QCD combined with experimental measurements.  Currently $\vert V_{us} \vert$ is determined from the kaon decay widths and a lattice calculation of the associated form factor. In this proceeding, I will present preliminary work on a lattice determination of the hyperon mass spectrum. I will additionally summarize future goals in which we will calculate the hyperon transition matrix elements, which will provide an alternative means for accessing $\vert V_{us} \vert$. This work is based on a particular formulation of SU(2) chiral perturbation theory for hyperons; determining the extent to which this effective field theory converges is instrumental in understanding the limits of its predictive power, especially since some hyperonic observables are difficult to calculate near the physical pion mass (e.g., hyperon-to-nucleon form factors), and thus the use of heavier than physical pion masses is likely to yield more precise results when combined with extrapolations to the physical point.
